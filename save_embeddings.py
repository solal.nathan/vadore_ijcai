# SPDX-License-Identifier: GPL-3.0-or-later
"""
Save scaled user / item features and embeddings
"""

import os
import pickle

import numpy as np
import torch
from sklearn import datasets
from sklearn.preprocessing import StandardScaler

from nn_utils import BilinearForm, ReluTanhEncoder

xing_path = "./recsys2017.pub/"
model_dir = "./output/muse0/"
destdir = "./output/embeddings_and_features/"
os.mkdir(destdir)

dropoutnet_feature_path = xing_path + "eval/"
dropoutnet_path = xing_path + "eval/warm/"

user_feature_path = dropoutnet_feature_path + "user_features_0based.txt"
item_feature_path = dropoutnet_feature_path + "item_features_0based.txt"

user_features, _ = datasets.load_svmlight_file(
    user_feature_path, zero_based=True, dtype=np.float32
)
user_features = user_features.tolil().toarray()

item_features, _ = datasets.load_svmlight_file(
    item_feature_path, zero_based=True, dtype=np.float32
)
item_features = item_features.tolil().toarray()

with open(model_dir + "/user_scaler.pkl", "rb") as f:
    user_scaler = pickle.loads(f.read())

user_features = user_scaler.transform(user_features)
user_features = torch.Tensor(user_features)

with open(model_dir + "/item_scaler.pkl", "rb") as f:
    item_scaler = pickle.loads(f.read())

item_features = item_scaler.transform(item_features)
item_features = torch.Tensor(item_features)

device = torch.device("cuda:0")

encoder_user = torch.load(model_dir + "/encoder_user.pt")
encoder_user.eval()
encoder_item = torch.load(model_dir + "/encoder_item.pt")
encoder_item.eval()
bilinear = torch.load(model_dir + "/bilinear.pt")
bilinear.eval()

A = list(bilinear.parameters())[0].detach().cpu().numpy().squeeze()

encoder_user = encoder_user.to(device)
encoder_item = encoder_item.to(device)
bilinear = bilinear.to(device)

with torch.no_grad():
    final_embedding = [
        encoder_item.forward(item_features[z].float().to(device))
        for z in np.array_split(
            np.arange(item_features.shape[0]), int(item_features.shape[0] / 10000)
        )
    ]

final_embedding = np.vstack([x.cpu().numpy() for x in final_embedding])
final_embedding = np.matmul(A, final_embedding.T)

with torch.no_grad():
    users_embedding = [
        encoder_user.forward(user_features[z].float().to(device))
        for z in np.array_split(
            np.arange(user_features.shape[0]), int(user_features.shape[0] / 10000)
        )
    ]
users_embedding = np.vstack([x.cpu().numpy() for x in users_embedding])

# Save user embeddings
np.save(destdir + "user_features.npy", user_features.numpy())
np.save(destdir + "user_embeddings.npy", users_embedding)

# Save item embeddings
np.save(destdir + "offer_features", item_features.numpy())
np.save(destdir + "offer_embeddings.npy", final_embedding)
