# SPDX-License-Identifier: GPL-3.0-or-later
"""
Computes and saves top-1000 recommendations on the training set according to the
Muse.0 modules.
These top-1000 recommendations are later used for training the Muse.1 module
"""

import os
import time

import faiss
import numpy as np
import pandas as pd

xing_path = "./recsys2017.pub/"  # TO MODIFY
model_dir = xing_path + "./output/muse0/"
destdir = "./output/muse1train/"
os.mkdir(destdir)

dropoutnet_feature_path = "./output/embeddings_and_features/"
train_path = xing_path + "eval/warm/train.csv"

user_embedding_path = dropoutnet_feature_path + "user_embeddings.npy"
offer_embedding_path = dropoutnet_feature_path + "offer_embeddings.npy"


train = pd.read_csv(train_path)
train.columns = ["uid", "iid", "inter", "date"]
train = train.sort_values("inter", ascending=False).drop_duplicates(["uid", "iid"])
print(train.shape)

unq_users = pd.Index(train.uid.unique())

user_embedding = np.load(user_embedding_path)
offer_embedding = np.load(offer_embedding_path).T
print(user_embedding.shape, offer_embedding.shape)
# Save top1k / train
index_exhaustive = faiss.IndexFlatIP(offer_embedding.shape[1])
index_exhaustive.add(-offer_embedding)

NMAX = 1000

s0 = time.time()
scores, top_ads = index_exhaustive.search(user_embedding[unq_users.to_numpy()], NMAX)
s1 = time.time()
print(s1 - s0)

s2 = time.time()
to_validate = train.groupby("uid")["iid"].apply(set)
kept = []
for uid, ads in zip(to_validate.index, to_validate):
    i = unq_users.get_loc(uid)
    intersection = ads.intersection(top_ads[i])
    if intersection:
        for job_ad in intersection:
            where = np.where(top_ads[i] == job_ad)[0][0]
            kept += [[uid, job_ad, where, scores[i][where], i]]

kept = pd.DataFrame(kept, columns=["uid", "iid", "rank", "score", "loc_in_scores"])
s3 = time.time()
print(s3 - s2)
print(kept)

kept = (
    kept.set_index(["uid", "iid"]).join(train.set_index(["uid", "iid"])).reset_index()
)
print(kept)

kept.to_csv(destdir + "train.csv", index=False)
np.save(destdir + "scores.npy", scores)
np.save(destdir + "top_ads.npy", top_ads)
