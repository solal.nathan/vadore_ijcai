# SPDX-License-Identifier: GPL-3.0-or-later
"""
Created on Thu Feb 16 13:51:15 2023

@author: gbied
"""

import torch


class Head(torch.nn.Module):
    def __init__(self, input_dim):
        super(Head, self).__init__()
        self.linear1 = torch.nn.Linear(input_dim, input_dim)
        self.linear2 = torch.nn.Linear(input_dim, input_dim)
        self.final = torch.nn.Linear(input_dim, 1)

    def forward(self, x):
        x = torch.relu(self.linear1.forward(x))
        x = torch.relu(self.linear2.forward(x))
        preds = self.final(x)

        return preds


class HeadCombiner(torch.nn.Module):
    def __init__(self, input_dim, intermediary_dim):
        super(HeadCombiner, self).__init__()
        self.linear = torch.nn.Linear(input_dim, intermediary_dim)
        self.final = torch.nn.Linear(intermediary_dim, 1)

    def forward(self, x):
        x = torch.relu(self.linear.forward(x))
        preds = self.final(x)

        return preds


class ThreeHeadRecommender(torch.nn.Module):
    def __init__(self, xshape, yshape, intermediary_dim):
        super(ThreeHeadRecommender, self).__init__()
        self.linear_x = torch.nn.Linear(xshape, intermediary_dim)
        self.linear_y = torch.nn.Linear(yshape, intermediary_dim)

        self.hiring_head = Head(intermediary_dim * 3)
        self.application_head = Head(intermediary_dim * 3)
        self.popularity_head = Head(intermediary_dim)

        self.head_combiner = HeadCombiner(3, 100)

    def forward(self, x, y):
        x = self.linear_x(x)
        y = self.linear_y(y)
        z = torch.cat([x, y, x * y], axis=1)

        hiring_prediction = self.hiring_head.forward(z)
        application_prediction = self.application_head.forward(z)
        popularity_prediction = self.popularity_head.forward(y)

        final_pred = torch.cat(
            [hiring_prediction, application_prediction, popularity_prediction], axis=1
        )
        final_pred = self.head_combiner.forward(final_pred)

        return (
            final_pred,
            hiring_prediction,
            application_prediction,
            popularity_prediction,
        )


class SingleHeadRecommender(torch.nn.Module):
    def __init__(self, xshape, yshape, intermediary_dim):
        super(SingleHeadRecommender, self).__init__()
        self.linear_x = torch.nn.Linear(xshape, intermediary_dim)
        self.linear_y = torch.nn.Linear(yshape, intermediary_dim)

        self.head = Head(intermediary_dim * 3)

    def forward(self, x, y):
        x = self.linear_x(x)
        y = self.linear_y(y)
        z = torch.cat([x, y, x * y], axis=1)

        prediction = self.head.forward(z)

        return prediction


class TwoHeadRecommenderAppli(torch.nn.Module):
    def __init__(self, xshape, yshape, intermediary_dim):
        super(TwoHeadRecommenderAppli, self).__init__()
        self.linear_x = torch.nn.Linear(xshape, intermediary_dim)
        self.linear_y = torch.nn.Linear(yshape, intermediary_dim)

        self.hiring_head = Head(intermediary_dim * 3)
        self.application_head = Head(intermediary_dim * 3)

        self.head_combiner = HeadCombiner(2, 100)

    def forward(self, x, y):
        x = self.linear_x(x)
        y = self.linear_y(y)
        z = torch.cat([x, y, x * y], axis=1)

        hiring_prediction = self.hiring_head.forward(z)
        application_prediction = self.application_head.forward(z)

        final_pred = torch.cat([hiring_prediction, application_prediction], axis=1)
        final_pred = self.head_combiner.forward(final_pred)

        return final_pred, hiring_prediction, application_prediction


class TwoHeadRecommenderPopularity(torch.nn.Module):
    def __init__(self, xshape, yshape, intermediary_dim):
        super(TwoHeadRecommenderPopularity, self).__init__()
        self.linear_x = torch.nn.Linear(xshape, intermediary_dim)
        self.linear_y = torch.nn.Linear(yshape, intermediary_dim)

        self.hiring_head = Head(intermediary_dim * 3)
        self.popularity_head = Head(intermediary_dim)

        self.head_combiner = HeadCombiner(2, 100)

    def forward(self, x, y):
        x = self.linear_x(x)
        y = self.linear_y(y)
        z = torch.cat([x, y, x * y], axis=1)

        hiring_prediction = self.hiring_head.forward(z)
        popularity_prediction = self.popularity_head.forward(y)

        final_pred = torch.cat([hiring_prediction, popularity_prediction], axis=1)
        final_pred = self.head_combiner.forward(final_pred)

        return final_pred, hiring_prediction, popularity_prediction


class PersonalizedHeadCombiner(torch.nn.Module):
    def __init__(self, input_dim, context_dim, intermediary_dim):
        super(PersonalizedHeadCombiner, self).__init__()
        self.linear = torch.nn.Linear(input_dim, intermediary_dim)
        self.context_embedding = torch.nn.Linear(context_dim, intermediary_dim)
        self.final = torch.nn.Linear(intermediary_dim, 1)

    def forward(self, x, context):
        x = torch.relu(self.linear.forward(x))
        context = self.context_embedding.forward(context)
        x = x * context
        preds = self.final(x)

        return preds


class PersonalizedThreeHeadRecommender(torch.nn.Module):
    def __init__(self, xshape, yshape, intermediary_dim):
        super(PersonalizedThreeHeadRecommender, self).__init__()
        self.linear_x = torch.nn.Linear(xshape, intermediary_dim)
        self.linear_y = torch.nn.Linear(yshape, intermediary_dim)

        self.hiring_head = Head(intermediary_dim * 3)
        self.application_head = Head(intermediary_dim * 3)
        self.popularity_head = Head(intermediary_dim)

        self.head_combiner = PersonalizedHeadCombiner(3, intermediary_dim, 100)
        self.final_agregator = torch.nn.Linear(xshape, 100)

    def forward(self, x, y):
        x = self.linear_x(x)
        y = self.linear_y(y)
        z = torch.cat([x, y, x * y], axis=1)

        hiring_prediction = self.hiring_head.forward(z)
        application_prediction = self.application_head.forward(z)
        popularity_prediction = self.popularity_head.forward(y)

        final_pred = torch.cat(
            [hiring_prediction, application_prediction, popularity_prediction], axis=1
        )
        final_pred = self.head_combiner.forward(final_pred, x)

        return (
            final_pred,
            hiring_prediction,
            application_prediction,
            popularity_prediction,
        )
