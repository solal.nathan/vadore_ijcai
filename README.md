# Muse

Repository for the experiments on the RecSys 2017 Challenge dataset in the context of the [IJCAI 2023 submission "Toward Job Recommendation for All"](https://ijcai-23.org/special-track-on-ai-for-good) (AI4SG5817).

## Requirements

The version of the packages used for the computational experiments are described in the `requirements.txt` file.

To setup a virtual environment and download the required packages, you may for instance use the `virtualenv` module from the python standard 
library and follow the next instructions:


Create a virtualenv named `venv`

```
python3 -m venv venv
```


Activate it

```
source bin/activate
```


Install packages from the requirements file

```
pip3 install -r requirements.txt
```

You will also need to install [faiss](https://github.com/facebookresearch/faiss/blob/main/INSTALL.md) which is not available in pip. We recommend using conda for this.

## Replication of the experiments

Download the data files from the DropoutNet (Volkovs et al., 2017) Paper [github repository](https://github.com/layer6ai-labs/DropoutNet); extract them to form a `recsys2017.pub/` folder. 

You can modify the paths to match your installation if it is different from ours.

<details>
<summary>You can use grep to indentify all files and lines to modify</summary>

```bash
grep -n "#TO MODIFY" *
```
</details>

---

Train the `Muse.0` module (saving the `Muse.0` standard scalers in directory muse0):
```
python train_muse0.py
```

Save the `Muse.0` top-1000 ranks for each of the train uids with matches:
```
python setup_muse1_training.py
```

Train the `Muse.1` module (saving the models in file muse1):
```
python train_muse1.py
```

Compute ranks (those less than 100, those above 100 are turned into Inf to avoid exhaustive sorting of recommendations) for the `Muse.1` module, and save them:

* In the user cold start case (uncomment only the line with `user_cold_ranks.csv`):
```
python eval_muse1.py
```
* In the warm start case (uncomment only the line with `warm_ranks.csv`):
```
python eval_muse1.py
```

The exact same process apply for `Muse.2`:

```
python train_muse2.py
```
```
python eval_muse2.py
```

You can also evaluate using the siginificativity script if you want to run a t-test to assure statistical significativy (we report p << 0.01).

## Results

Running the above training for the 3 modules might take up to 12 hours and require up to 14G of VRAM. Using the seeds given in the code should yield these results:

| Recall@100 | Muse.0 | Muse.1 | Muse.2 |
| ---------- | -----  | ------ | ------ |
| Warm start | 0.131  | 0.249  | 0.249  |
| Cold start | 0.123  | 0.233  | 0.240  |


## Pre-trained Weights

You can download the already trained weights for `Muse.0`, `Muse.1` and `Muse.2` on [zenodo](https://zenodo.org/record/8056293).
You can extract them at the root of the git repostory to obtain the original file structure.

## License

GNU General Public License v3.0 or later

See COPYING to see the full text.
