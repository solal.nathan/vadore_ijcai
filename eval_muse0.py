# SPDX-License-Identifier: GPL-3.0-or-later
"""
Evaluation script for the Muse.0 module.
"""

import numpy as np
import pandas as pd
import torch
import faiss

xing_path = "./recsys2017.pub/"

dropoutnet_feature_path = "./output/embeddings_and_features/"
test_path = xing_path + "eval/warm/test_warm.csv"
#test_path = xing_path + "eval/warm/test_cold_user.csv"

full_test = pd.read_csv(test_path)
print(full_test)
full_test.columns = ['uid', 'iid', 'inter', 'date']

full_test = full_test.drop_duplicates(['uid', 'iid'])
print(full_test.uid.nunique())
unique_test_ids = pd.Index(full_test.uid.unique())


user_embedding_path = dropoutnet_feature_path + 'user_embeddings.npy'
offer_embedding_path = dropoutnet_feature_path + 'offer_embeddings.npy'

stage1_user_embedding = np.load(user_embedding_path)
stage1_item_embedding = np.load(offer_embedding_path).T

index_exhaustive = faiss.IndexFlatIP(stage1_item_embedding.shape[1])
index_exhaustive.add(-stage1_item_embedding)

final_k = 100

device = torch.device('cpu')

successes = []

chunks = np.array_split(unique_test_ids.to_numpy(),
                        int(len(unique_test_ids.to_numpy()) / 1000))

recos = {}
for _, chunk in enumerate(chunks):
    print("Chunk", _)
    scores, top_ads = index_exhaustive.search(stage1_user_embedding[chunk], final_k)

    corr_ids = [list(top_ads[i]) for i in range(len(scores))]

    for uid, recs in zip(chunk, corr_ids):
        recos[uid] = recs

    throwaway = full_test.loc[full_test.uid.isin(chunk)].copy()
    throwaway['success'] = [iid in recos[uid] for uid, iid in zip(throwaway.uid, throwaway.iid)]
    print(throwaway.groupby('uid')['success'].mean().mean())

full_test['success'] = [iid in recos[uid] for uid, iid in zip(full_test.uid, full_test.iid)]

recall = full_test.groupby('uid')['success'].mean().mean()
print(f"recall@{final_k} = {recall}")
