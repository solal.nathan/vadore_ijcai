# SPDX-License-Identifier: GPL-3.0-or-later
"""
Evaluation script for the Muse.1 module.
"""

import datetime as dt
import gc
import pickle
import time

import faiss
import numpy as np
import pandas as pd
import torch
from scipy.stats import rankdata
from tqdm import tqdm

from nn_utils import BilinearForm, ReluTanhEncoder
from reranker_utils_multihead import *


xing_path = "./recsys2017.pub/"
muse1_dir = "./output/muse1/"

dropoutnet_feature_path = "./output/embeddings_and_features/"
test_path = xing_path + "eval/warm/test_warm.csv"
test_path = xing_path + "eval/warm/test_cold_user.csv"
test_item_path = (
    xing_path + "eval/warm/test_cold_user_item_ids.csv"
)

full_test = pd.read_csv(test_path)
print(full_test)
full_test.columns = ["uid", "iid", "inter", "date"]

full_test = full_test.drop_duplicates(["uid", "iid"])
print(full_test.uid.nunique())
unique_test_ids = pd.Index(full_test.uid.unique())


user_embedding_path = dropoutnet_feature_path + "user_embeddings.npy"
offer_embedding_path = dropoutnet_feature_path + "offer_embeddings.npy"

user_feature_path = dropoutnet_feature_path + "user_features.npy"
offer_feature_path = dropoutnet_feature_path + "offer_features.npy"


model = torch.load(muse1_dir + "/mlp_reranking.pt")
model.eval()

stage1_user_embedding = np.load(user_embedding_path)
stage1_item_embedding = np.load(offer_embedding_path).T


offer_representations = np.load(offer_feature_path)
offers_all = np.hstack([offer_representations, stage1_item_embedding])
del offer_representations  # , offer_embeddings

jobseeker_representations = np.load(user_feature_path)
jobseeker_all = np.hstack([jobseeker_representations, stage1_user_embedding])
del jobseeker_representations  # , jobseeker_embeddings

with open(muse1_dir + "/user_scaler.pkl", "rb") as f:
    user_scaler = pickle.loads(f.read())
with open(muse1_dir + "/item_scaler.pkl", "rb") as f:
    item_scaler = pickle.loads(f.read())
with open(muse1_dir + "/cross_scaler.pkl", "rb") as f:
    cross_scaler = pickle.loads(f.read())

offers_all = item_scaler.transform(offers_all)
jobseeker_all = user_scaler.transform(jobseeker_all)

index_exhaustive = faiss.IndexFlatIP(stage1_item_embedding.shape[1])
index_exhaustive.add(-stage1_item_embedding)

NMAX = 1000
final_k = 100

device = torch.device("cpu")

successes = []


chunks = np.array_split(
    unique_test_ids.to_numpy(), int(len(unique_test_ids.to_numpy()) / 1000)
)

recos = {}
for _, chunk in enumerate(chunks):
    print("Chunk", _)

    print("FAISS lookup")
    t0 = time.perf_counter()

    scores, top_ads = index_exhaustive.search(stage1_user_embedding[chunk], NMAX)
    t1 = time.perf_counter()
    print((t1 - t0) / len(scores))

    print("Muse.1")
    user_ix = np.repeat(chunk, NMAX)
    item_ix = top_ads.ravel()
    ranks = np.repeat(np.arange(NMAX), len(chunk))
    scores_flat = scores.ravel()
    cross = torch.Tensor(
        cross_scaler.transform(
            np.hstack([ranks.reshape(-1, 1), scores_flat.reshape(-1, 1)])
        )
    )

    model = model.to(device)
    with torch.no_grad():
        preds = model.forward(
            torch.Tensor(jobseeker_all[user_ix]).to(device),
            torch.cat([torch.Tensor(offers_all[item_ix]), cross], axis=1).to(device),
        )

    preds = preds.cpu().numpy()
    preds = preds.reshape((len(chunk), NMAX))
    ranks = np.argpartition(-preds, final_k, axis=1)[:, :final_k]
    corr_ids = [list(top_ads[i, ranks[i, :]]) for i in range(len(ranks))]

    for uid, recs in zip(chunk, corr_ids):
        recos[uid] = recs
    t2 = time.perf_counter()
    print((t2 - t1) / len(scores))
    throwaway = full_test.loc[full_test.uid.isin(chunk)].copy()
    throwaway["success"] = [
        iid in recos[uid] for uid, iid in zip(throwaway.uid, throwaway.iid)
    ]
    print(throwaway.groupby("uid")["success"].mean().mean())

full_test["success"] = [
    iid in recos[uid] for uid, iid in zip(full_test.uid, full_test.iid)
]

# success = [ad in corr_ids[i] for i, ad in enumerate(test.iid)]
# print(np.mean(np.array(success)))

# test['success'] = success
final_score = full_test.groupby("uid")["success"].mean().mean()
print(f"Recall@{final_k} = {final_score}")
