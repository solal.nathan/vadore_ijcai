# SPDX-License-Identifier: GPL-3.0-or-later
"""
Train Muse.1 (single head)
"""


import ovads
import pickle

import numpy as np
import pandas as pd
import torch
from sklearn.preprocessing import StandardScaler
from tqdm import tqdm

from reranker_utils_multihead import SingleHeadRecommender

xing_path = "./recsys2017.pub/"  # TO MODIFY
model_dir = "./output/muse0/"
destdir = "./output/muse1/"
os.mkdir(destdir)

dropoutnet_feature_path = "./output/embeddings_and_features/"
train_path = "./output/muse1train/"

user_embedding_path = dropoutnet_feature_path + "user_embeddings.npy"
offer_embedding_path = dropoutnet_feature_path + "offer_embeddings.npy"
user_feature_path = dropoutnet_feature_path + "user_features.npy"
offer_feature_path = dropoutnet_feature_path + "offer_features.npy"

train = pd.read_csv(train_path + "train.csv")
train = train.loc[train.inter != 0]
print(train.shape)

unq_users = pd.Index(train.uid.unique())

offer_representations = np.load(offer_feature_path)
offer_embeddings = np.load(offer_embedding_path).T
offers_all = np.hstack([offer_representations, offer_embeddings])
del offer_representations, offer_embeddings

jobseeker_representations = np.load(user_feature_path)
jobseeker_embeddings = np.load(user_embedding_path)
jobseeker_all = np.hstack([jobseeker_representations, jobseeker_embeddings])
del jobseeker_representations, jobseeker_embeddings

scores = np.load(train_path + "scores.npy")
top_ads = np.load(train_path + "top_ads.npy")

user_scaler = StandardScaler()
jobseeker_all = user_scaler.fit_transform(jobseeker_all)
pickle.dump(user_scaler, open(destdir + "user_scaler.pkl", "wb"))
jobseeker_all = torch.Tensor(jobseeker_all)

item_scaler = StandardScaler()
offers_all = item_scaler.fit_transform(offers_all)
pickle.dump(item_scaler, open(destdir + "item_scaler.pkl", "wb"))
offers_all = torch.Tensor(offers_all)

cross_scaler = StandardScaler()
cross_scaling_input = np.hstack(
    [
        np.repeat(np.arange(1000), len(scores)).reshape(-1, 1),
        scores.ravel().reshape(-1, 1),
    ]
)
cross_scaler.fit(cross_scaling_input)

if torch.cuda.is_available():
    device = torch.device("cuda")
else:
    device = torch.device("cpu")

np.random.seed(42)
torch.manual_seed(42)

LEARNING_RATE = 0.001
INTERMEDIARY_DIM = 200
NUM_EPOCH = 50
BATCH_SIZE = 128

model = SingleHeadRecommender(
    jobseeker_all.shape[1], offers_all.shape[1] + 2, INTERMEDIARY_DIM
)
model = model.to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=LEARNING_RATE)

logit_crit = torch.nn.BCEWithLogitsLoss(reduce=False)

train_losses = []
for epoch in range(NUM_EPOCH):
    print("Epoch", epoch)
    running_total_loss = 0.0

    neg_selection = np.random.choice(1000, len(train))
    neg_scores = scores[train.loc_in_scores.to_numpy(), neg_selection]
    neg_iids = top_ads[train.loc_in_scores.to_numpy(), neg_selection]
    negcross = cross_scaler.transform(
        np.hstack([neg_selection.reshape(-1, 1), neg_scores.reshape(-1, 1)])
    )
    negcross = torch.Tensor(negcross)

    poscross = cross_scaler.transform(
        np.hstack(
            [
                train["rank"].to_numpy().reshape(-1, 1),
                train["score"].to_numpy().reshape(-1, 1),
            ]
        )
    )
    poscross = torch.Tensor(poscross)

    base_batch_idx = np.arange(len(train))
    batches = np.array_split(base_batch_idx, int(len(train) / BATCH_SIZE))

    for ix in tqdm(batches):
        batch_data = train.iloc[ix]

        anchors_ix = np.array(batch_data.uid)
        pos_ix = np.array(batch_data.iid)
        neg_ix = neg_iids[ix]

        anchors = jobseeker_all[anchors_ix].to(device)
        pos = offers_all[pos_ix].to(device)
        pos = torch.cat([pos, poscross[ix].to(device)], axis=1)
        neg = offers_all[neg_ix].to(device)
        neg = torch.cat([neg, negcross[ix].to(device)], axis=1)

        pos_labels = torch.ones(len(anchors_ix)).reshape(-1, 1).to(device)
        neg_labels = torch.zeros(len(anchors_ix)).reshape(-1, 1).to(device)

        optimizer.zero_grad()

        preds_pos = model.forward(anchors, pos)
        preds_neg = model.forward(anchors, neg)

        total_loss = 0.0

        total_loss += logit_crit(preds_pos.reshape(-1, 1), pos_labels).sum()
        total_loss += logit_crit(preds_neg.reshape(-1, 1), neg_labels).sum()

        total_loss.backward()
        optimizer.step()
        running_total_loss += total_loss.item()

    train_losses += [running_total_loss / len(train)]
    print(train_losses[-1])

losses = pd.DataFrame(train_losses, columns=["total"])
losses.to_csv(destdir + "losses.csv")

torch.save(model, destdir + "mlp_reranking.pt")
with open(destdir + "cross_scaler.pkl", "wb") as file:
    pickle.dump(cross_scaler, file)
with open(destdir + "user_scaler.pkl", "wb") as file:
    pickle.dump(user_scaler, file)
with open(destdir + "item_scaler.pkl", "wb") as file:
    pickle.dump(item_scaler, file)
